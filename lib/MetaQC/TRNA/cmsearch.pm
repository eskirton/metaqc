=pod

=head1 NAME

MetaQC::TRNA::cmsearch

=head1 DESCRIPTION

Run cmsearch, filter results, save summary in object for querying.  Optionally cmsearch results can be provided, otherwise cmsearch shall be run. 

=head1 METHODS

=over 5

=cut

package MetaQC::TRNA::cmsearch;

use strict;
use JSON;
use File::Path qw(make_path remove_tree);
use File::Slurp;
use File::Which;
use File::Copy;

=item new

Constructor requires output folder and contigs FASTA as 'dir' and 'contigs' params respectively.  cmsearch results may also be provided as 'input' parameter.

=cut

sub new
{
	my ($class, $config, $dir, $contigsFile, $threads, $infile) = @_;
	defined($config) or die("Config required\n");
	$dir or die("Dir required\n");
	$contigsFile or die("Contigs file required\n");
	$threads or die("Threads required\n");
	exists($config->{tRNA}->{highMinTRnas}) or die("Config missing tRNA/highMinTRnas\n");
	exists($config->{tRNA}->{db}) or die("Config tRNA cmsearch model db\n");
	which('cmsearch') or die("cmsearch executable not found\n");

	-d $dir or make_path($dir) or die("Unable to mkdir $dir: $!\n");
	my $cmsearchFile = "$dir/tRNA.cmsearch";
	my $tmpdir = "$dir/tRNAscan";

	my $this = 
	{
		config => $config,
		dir => $dir,
		tmpdir => $tmpdir,
		contigsFile => $contigsFile,
		cmsearchFile => $cmsearchFile,
		threads => $threads,
		tRNA => {},
		infile => $infile,
		modelFile => $config->{tRNA}->{db}
	};
	bless $this, $class;

	# LOAD ANY EXISTING RESULTS
	$infile and $this->importCmsearch($infile);
	$this->_runCmsearch;
	$this->load;
	return $this;
}

=item importCmsearch

Import cmsearch results if they exist.  Must use compatible model names.

=cut

sub importCmsearch
{
	my ($this, $infile) = @_;
	my $outfile = $this->{cmsearchFile};
	if ( -e $outfile )
	{
		die("Cannot import cmsearch file as a cmsearch results file already exists: $outfile\n");
	}
	# TODO VERIFY PROVIDED FILE IS VALID FORMAT
	print "Copying $infile -> $outfile\n";
	copy($infile, $outfile);
}

=item _runCmsearch

Run cmsearch to find tRNAs in all contigs, filter results, and save loci in hash.

=cut

sub _runCmsearch
{
	my ($this) = @_;
	my $contigsFile = $this->{contigsFile};
	my $cmsearchFile = $this->{cmsearchFile};
	-s $cmsearchFile and return;
	my $modelFile = $this->{modelFile};
	my $tmpfile = "$cmsearchFile.tmp";
	my @cmd = qq(cmsearch -o /dev/null --cpu $this->{threads} --tblout $tmpfile --notextw --cut_tc $modelFile $contigsFile);
	print "Executing: @cmd\n";
	system(@cmd) == 0 or die("ERROR: cmsearch FAILED: $!\n@cmd\n");
	move($tmpfile, $cmsearchFile);
}

=item load

Load tRNAs from cmsearch file

=cut

sub load
{
	my ($this) = @_;

	my %results;
	my $cmsearchFile = $this->{cmsearchFile};

	# LOAD GENES AND STORE BY CONTIG AND LEFT POS.  WHEN MORE THAN ONE ANNOTATION EXISTS FOR A LEFT POS,
	# ONLY THE HIGHEST SCORING ANNOTATION IS KEPT.  THERE COULD STILL BE OVERLAPPING ANNOTATIONS, WHICH
	# ARE RESOLVED IN THE NEXT BLOCK
	my %loci;
	print "Reading $cmsearchFile\n";
	open(my $in, '<', $cmsearchFile) or die($!);
	while (<$in>)
	{
		chomp;
		next if /^#/;
		my @row = split(/\s+/);
		my ($contig, $name, $start, $stop, $strand, $score) = @row[0,2,7,8,9,14];
		my ($left, $right) = $start < $stop ? ($start, $stop) : ($stop, $start);

		# SAVE BY CONTIG AND LEFT POS
		exists($loci{$contig}) or $loci{$contig}={};
		if ( exists($loci{$contig}->{$left}) )
		{
			# REPLACE EXISTING ANNOT IF THIS ONE HAS HIGHER SCORE
			if ( $score > $loci{$contig}->{$left}->[0] )
			{
				$loci{$contig}->{$left} = [ $score, $right, $name, $strand ];
			}
		} else
		{
			$loci{$contig}->{$left} = [ $score, $right, $name, $strand ];
		}
	}
	close($in);
	
	# COUNT ONLY HIGHEST-SCORING OF OVERLAPPING ANNOTATIONS
	my %results; # contig => numTrna
	foreach my $contig ( sort keys %loci )
	{
		# RESULTS
		my $numTrna = 0;

		# PREVIOUS RECORD
		my $prevLeft;
		my $prevScore;
		my $prevRight;
		my $prevName;
		my $prevStrand;

		# PROCESS FROM LEFT TO RIGHT
		foreach my $left ( sort { $a<=>$b } keys %{$loci{$contig}} )
		{
			my ($score, $right, $name, $strand) = @{$loci{$contig}->{$left}};
			if ( !defined($prevLeft) )
			{
				1; # FIRST ANNOT ON THIS CONTIG
			}
			elsif ( $left < $prevRight )
			{
				# CURRENT OVERLAPS PREVIOUS; KEEP HIGHEST SCORING
				$score < $prevScore and next;
			} else
			{
				# CURRENT DOES NOT OVERLAP PREVIOUS, SAVE PREVIOUS
				++$numTrna;
			}

			# SAVE CURRENT
			$prevLeft=$left;
			$prevRight=$right;
			$prevScore=$score;
			$prevName=$name;
			$prevStrand=$strand;
		}
		# COUNT LAST
		$prevScore and ++$numTrna;

		# SAVE RESULTS
		$results{$contig} = $numTrna;
	}
	$this->{tRNA} = \%results;
}

=item countNumTRnas

Given list of contig IDs, return the total number of tRNAs.  This class doesn't consider tRNA anticodons.

=cut

sub countNumTRnas
{
	my ($this, $contigIds) = @_;
	my $sum = 0;
	foreach my $contigId (@$contigIds)
	{
		exists($this->{tRNA}->{$contigId}) and $sum += $this->{tRNA}->{$contigId};
	}
	return $sum;
}

=item mostlyComplete

Given list of contig IDs, return true if has the minimum number of different tRNAs specified in the config file.

=cut

sub mostlyComplete
{
	my ($this, $contigIds) = @_;
	my $numTRnas = $this->countNumTRnas($contigIds);
	my $result = 0;
	if ( $numTRnas ne '?' and $numTRnas >= $this->{config}->{tRNA}->{highMinTRnas} ) { $result=1 }
	return wantarray ? ($result, $numTRnas) : $result;
}

=item search

Validate cmsearch tRNAs using tRNAscan-SE.  Requires contig ID and domain for models to use.

=cut

sub search
{
	my ($this) = @_;
	1;
}

=item getSubseq


=cut

sub getSubseqs
{

1;

}

1;

=back

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 COPYRIGHT/LICENSE

Copyright 2017 US DOE Joint Genome Institute.  Use freely under the same terms as Perl itself.

=head1 AUSPICE STATEMENT

The work conducted by the U.S. Department of Energy Joint Genome Institute is supported by the Office of Science of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

=cut
