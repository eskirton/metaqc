=pod

=head1 NAME

MetaQC::CheckM

=head1 DESCRIPTION

Evaluate metagenome bin completeness and contamination using CheckM.  Each bin is analyzed separately and the results are cached, which may avoid recomputation when trying multiple binning parameters (i.e. MetaBAT GA optimization).

=head1 METHODS

=over 5

=cut

package MetaQC::CheckM;

use strict;
use Digest::MD5 qw(md5_hex);
use File::Path qw(make_path remove_tree);
use JSON;
use File::Slurp;
use File::Basename;
use Parallel::ForkManager;
use Env qw(TMPDIR SLURM_TMPDIR NERSC_HOST HOSTNAME);

our $json = new JSON;

=item new

Constructor

=cut

sub new
{
	my ($class, $config, $dir, $threads, $ram) = @_;
	$config or die("config required\n");
	$dir or die("dir required\n");
	-d $dir or make_path($dir) or die("Unable to mkdir $dir: $!\n");
	$threads or die("threads required\n");
	my $checkmFile = "$dir/checkm.json";
	unless ( defined($ram) and $ram >= 48 )
	{
		die("CheckM requires at least 48G of memory\n");
	}

	my $this =
	{
		config => $config,
		dir => $dir,
		checkmFile => $checkmFile,
		threads => $threads,
		results => {}
	};
	bless $this, $class;
	$this->_load;
	return $this;
}

=item _load

Load checkm results from json file and new folders.

=cut

sub _load
{
	my $this = shift;

	# LOAD ANY PREVIOUSLY COMPUTED RESULTS
	my $checkmFile = $this->{checkmFile};
	if ( -e $checkmFile )
	{
		print "Reading $checkmFile\n";
		my $txt = read_file($checkmFile);
		$this->{results} = $json->decode($txt);
	}

	# LOAD ANY RESULTS NOT IN JSON FILE I.E IMPORTED
	my $numNew = 0;
	my $dir = $this->{dir};
	opendir(DIR, $dir) or die("Unable to read dir $dir: $!\n");
	my @folders = grep { /^\w+$/ } readdir(DIR);
	closedir(DIR);
	foreach my $md5 (@folders)
	{
		exists($this->{results}->{$md5}) and next;
		my $file = "$dir/$md5/qa.txt";
		if ( $this->_loadCheckmResults($file, $md5) )
		{
			print "Importing checkm results: $file\n";
			++$numNew;
		} elsif ( -d "$dir/$md5" )
		{
			print "Purging $dir/$md5 folder (incomplete)\n";
			remove_tree("$dir/$md5");
		}
	}
	$numNew and $this->save;
	print "There are ", scalar(keys %{$this->{results}}), " CheckM results\n";
	return $numNew;
}

=item save

Save CheckM results to JSON file.

=cut

sub save
{
	my $this = shift;
	my $txt = $json->pretty->encode($this->{results});
	open(my $out, '>', $this->{checkmFile}) or die($!); # slurp write_file adds troublesome extra characters
	print $out $txt;
	close($out);
}

=item annotate

Annotate all bins

=cut

sub annotate
{
	my ($this, $dir) = @_;

	# POPULATE QUEUE
	my $numDone = 0;
	opendir(DIR, $dir) or die($!);
	my @files = map { "$dir/$_" } grep { /\d+\.fn?a$/ } readdir(DIR);
	closedir(DIR);
	my @queue;
	foreach my $file ( sort @files )
	{
		my $md5 = $this->_md5($file);
		if ( exists($this->{results}->{$md5}) )
		{
			++$numDone;
		} else
		{
			push @queue, $file;
		}
	}
	my $total = scalar(@queue);
	print "$numDone bins have CheckM results; $total to-do\n";
	$total or return;

	my $num = 0;
	foreach my $file (@queue)
	{
		++$num;
		my $start = time;
		print "BEGIN CHECKM TASK $num of $total: $file\n\n";
		$this->checkm($file);
		my $min = int((time - $start)/60+0.5);
		my $hr = int($min/60);
		$min -= ($hr*60);
		print "DONE AFTER $hr hr $min min\n\n";
	}
}

=item _md5

Given contigs FASTA file, return md5 hex checksum

=cut

sub _md5
{
	my ($this, $infile) = @_;
	my @contigIds;
	open(my $in, '<', $infile) or die("Unable to read $infile: $!\n");
	while (<$in>) { /^>(\S+)/ and push @contigIds, $1 }
	close($in);
	@contigIds = sort @contigIds;
	return md5_hex(join('', @contigIds));
}

=item checkm

Run CheckM on a single bin.  Requires bin FASTA infile.

=cut

sub checkm
{
	my ($this, $infile) = @_;
	$infile or die("Bin fasta file required\n");
	-e $infile or die("Bin contigs file not found: $infile\n");

	# GENERATE IDENTIFIER FROM CONCATENATED CONTIG IDS
	my $md5 = $this->_md5($infile);

	# IF THIS SET HAS BEEN EVALUATED PREVIOUSLY, USE THE CACHED RESULTS
	if ( exists($this->{results}->{$md5}) )
	{
		my ($completeness, $contamination) = @{$this->{results}->{$md5}};
		return ($completeness, $contamination);
	}

	# TODO WRITE TO TMPDIR INSTEAD

	# OTHERWISE QUEUE CHECKM
	my $tmpdir = "$this->{dir}/$md5";
	my $binId = fileparse($infile, '.fa', '.fna');
	print "Evaluting $binId ($md5) using CheckM => $tmpdir\n";
	-d $tmpdir and remove_tree($tmpdir);
	make_path($tmpdir) or die("Unable to mkdir $tmpdir: $!\n");
	symlink($infile, "$tmpdir/$binId.fa") or die("Error symlinking $infile -> $tmpdir: $!\n");

	# RUN CHECKM
	my $outdir = "$tmpdir/checkm";
	-d $outdir and remove_tree($outdir);
	make_path($outdir) or die("unable to mkdir $outdir: $!\n");
	my $cmd = "checkm lineage_wf -q -t $this->{threads} -x fa $tmpdir $outdir >/dev/null 2>/dev/null";
	unless ( system($cmd) == 0 )
	{
		remove_tree($tmpdir);
		die("ERROR: checkm lineage_wf FAILED: $!\n$cmd\n");
	}

	my $tmpfile = "$tmpdir/qa.txt";
	$cmd = "checkm qa -q -t $this->{threads} $outdir/lineage.ms $outdir > $tmpfile";
	unless ( system($cmd) == 0 )
	{
		remove_tree($tmpdir);
		die("ERROR: checkm qa FAILED: $!\n$cmd\n");
	}

	# LOAD, SAVE, AND RETURN RESULTS
	$this->_loadCheckmResults($tmpfile, $md5);
	$this->{config}->{CHECKM}->{keepFiles} or remove_tree($tmpdir);
	my ($completeness, $contamination) = @{$this->{results}->{$md5}};
	return ($completeness, $contamination);
}

=item _loadCheckmResults

Read CheckM results file and save in object and to json file.  There must be only one bin per file.

=cut

sub _loadCheckmResults
{
	my ($this, $infile, $md5) = @_;
	-e $infile or return 0;
	open(my $in, '<', $infile) or return 0;
	for (my $i=0; $i<3; ++$i ) { my $hdr = <$in> }
	my $line = <$in>;
	my @row = split(/\s+/, $line);
	close($in);
	my ($completeness, $contamination) = @row[13, 14];
	unless ( defined($completeness) and defined($contamination) )
	{
		die("Invalid CheckM results for $infile : $md5\n");
	}
	$this->{results}->{$md5} = [ $completeness, $contamination ];
	$this->save;
	return 1;
}

=item isHighQual

Returns true if bin is high qual.

=cut

sub isHighQual
{
	my ($this, $contigs, $file) = @_;
	$file or die("Bin FASTA file required\n");
	my $md5 = md5_hex(join('', sort @$contigs));
	exists($this->{results}->{$md5}) or $this->checkm($file);
	my ($completeness, $contamination) = @{$this->{results}->{$md5}};
	$completeness < $this->{config}->{CHECKM}->{highMinCompleteness} and return 0;
	$contamination > $this->{config}->{CHECKM}->{highMaxContamination} and return 0;
	return 1;
}

=item qual

Returns quality of bin (high/medium/low) based upon completeness and contamination only.

=cut

sub qual
{
	my ($this, $contigs, $file) = @_;
	my ($completeness, $contamination) = $this->stats($contigs, $file);
	my $qual = 'low';
	if ( $completeness >= $this->{config}->{CHECKM}->{highMinCompleteness} and
		$contamination <= $this->{config}->{CHECKM}->{highMaxContamination} )
	{
		$qual = 'high';
	} elsif ( $completeness >= $this->{config}->{CHECKM}->{medMinCompleteness} and
		$contamination <= $this->{config}->{CHECKM}->{medMaxContamination} )
	{
		$qual = 'medium';
	}
	return wantarray ? ($qual, $completeness, $contamination) : $qual;
}

=item stats

Returns completeness and contamination estimates.

=cut

sub stats
{
	my ($this, $contigs, $file) = @_;
	my $md5 = md5_hex(join('', sort @$contigs));
	exists($this->{results}->{$md5}) or $this->checkm($file);
	my ($completeness, $contamination) = @{$this->{results}->{$md5}};
	return ($completeness, $contamination);
}

1;

=back

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 COPYRIGHT/LICENSE

Copyright 2017 US DOE Joint Genome Institute.  Use freely under the same terms as Perl itself.

=head1 AUSPICE STATEMENT

The work conducted by the U.S. Department of Energy Joint Genome Institute is supported by the Office of Science of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

=cut
