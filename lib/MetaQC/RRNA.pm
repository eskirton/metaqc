=pod

=head1 NAME

MetaQC::RRNA

=head1 DESCRIPTION

Run cmsearch, filter results, save summary in JSON file.  As cmsearch is fast, it is expected to be run on the entire assembly (i.e. all contigs).

=head1 METHODS

=over 5

=cut

package MetaQC::RRNA;

use strict;
use File::Path qw(make_path remove_tree);
use File::Copy;

=item new

Constructor.  Requires output folder and contigs FASTA.  Optionally accepts cmsearch file (if already exists).

=cut

sub new
{
	my ($class, $config, $dir, $contigsFile, $threads, $infile) = @_;
	defined($config) or die("RRNA: config hashref required\n");
	ref($config) eq 'HASH' or die("RRNA: Config hashref required\n");
	$dir or die("RRNA: Dir required\n");
	$contigsFile or die("RRNA: Contigs file required\n");
	$threads or die("Threads required\n");

	exists($config->{rRNA}) or die("Config missing rRNA section\n");
	exists($config->{rRNA}->{db}) or die("Config missing rRNA reference db\n");

	-d $dir or make_path($dir) or die("Unable to mkdir $dir: $!\n");
	my $cmsearchFile = "$dir/rRNA.tsv";

	# NEW OBJ
	my $this =
	{
		config => $config,
		dir => $dir,
		contigsFile => $contigsFile,
		infile => $infile, # defined only if cmsearch was run offline
		cmsearchFile => $cmsearchFile, # cmsearch output
		rRNA => {}, # contig ID => [ lsu, ssu, tsu, domain ]
		threads => $threads,
		modelFile => $config->{rRNA}->{db}
	};
	bless $this, $class;

	# EITHER LOAD INFILE OR RUN CMSEARCH
	if ( $infile )
	{
		$this->_load($infile);
	} elsif ( -e $cmsearchFile )
	{
		$this->_load($cmsearchFile);
	} else
	{
		$this->_cmsearch;
	}

	return $this;
}

=item _cmsearch

Run cmsearch to find rRNAs in all contigs, filter results, and save in JSON hash

=cut

sub _cmsearch
{
	my ($this) = @_;
	my $contigsFile = $this->{contigsFile};
	my $cmsearchFile = $this->{cmsearchFile};
	my $modelFile = $this->{modelFile};
	my $tmpfile = "$cmsearchFile.tmp";
	my @cmd = qq(cmsearch -o /dev/null --cpu $this->{threads} --tblout $tmpfile --notextw --cut_tc $modelFile $contigsFile);
	print "Executing: @cmd\n";
	system(@cmd) == 0 or die("ERROR: cmsearch FAILED: $!\n@cmd\n");
	move($tmpfile, $cmsearchFile);
	$this->_load($cmsearchFile);
}

=item _load

Load cmsearch outfile, filter results, and populate hash, which is stored in object.

=cut

sub _load
{
	my ($this, $cmsearchFile) = @_;

	# LOAD GENES AND STORE BY CONTIG AND LEFT POS.  WHEN MORE THAN ONE ANNOTATION EXISTS FOR A LEFT POS,
	# ONLY THE HIGHEST SCORING ANNOTATION IS KEPT.  THERE COULD STILL BE OVERLAPPING ANNOTATIONS, WHICH
	# ARE RESOLVED IN THE NEXT BLOCK
	my %loci;
	print "Reading $cmsearchFile\n";
	open(my $in, '<', $cmsearchFile) or die($!);
	while (<$in>)
	{
		chomp;
		next if /^#/;
		my @row = split(/\s+/);
		my ($contig, $name, $start, $stop, $strand, $score) = @row[0,2,7,8,9,14];
		my ($left, $right) = $start < $stop ? ($start, $stop) : ($stop, $start);

		# SAVE BY CONTIG AND LEFT POS
		exists($loci{$contig}) or $loci{$contig}={};
		if ( exists($loci{$contig}->{$left}) )
		{
			# REPLACE EXISTING ANNOT IF THIS ONE HAS HIGHER SCORE
			if ( $score > $loci{$contig}->{$left}->[0] )
			{
				$loci{$contig}->{$left} = [ $score, $right, $name, $strand ];
			}
		} else
		{
			$loci{$contig}->{$left} = [ $score, $right, $name, $strand ];
		}
	}
	close($in);
	
	# COUNT ONLY HIGHEST-SCORING OF OVERLAPPING ANNOTATIONS
	my %results; # contig => [ numLsu, numSsu, numTsu, domain ]
	foreach my $contig ( sort keys %loci )
	{
		# RESULTS
		my $domain = undef;
		my $numLsu = 0;
		my $numSsu = 0;
		my $numTsu = 0;

		# PREVIOUS RECORD
		my $prevLeft;
		my $prevScore;
		my $prevRight;
		my $prevName;
		my $prevStrand;

		# PROCESS FROM LEFT TO RIGHT
		foreach my $left ( sort { $a<=>$b } keys %{$loci{$contig}} )
		{
			my ($score, $right, $name, $strand) = @{$loci{$contig}->{$left}};
			if ( !defined($prevLeft) )
			{
				1; # FIRST ANNOT ON THIS CONTIG
			}
			elsif ( $left < $prevRight )
			{
				# CURRENT OVERLAPS PREVIOUS; KEEP HIGHEST SCORING
				$score < $prevScore and next;
			} else
			{
				# CURRENT DOES NOT OVERLAP PREVIOUS, SAVE PREVIOUS
				if ( $prevName =~ /^(\w+)_rRNA_(\w+)$/ )
				{
					if ( $1 eq 'LSU' ) { ++$numLsu }
					elsif ( $1 eq 'SSU' ) { ++$numSsu }
					if ( !defined($domain) ) { $domain = $2 }
					elsif ( $2 ne $domain ) { $domain = 'INCONSISTENT' }
				} elsif ( $prevName eq '5S_rRNA' )
				{
					++$numTsu;
					if ( defined($domain) and $domain eq 'eukarya' ) { $domain = 'INCONSISTENT' }
				} elsif ( $prevName eq '5_8S_rRNA' )
				{
					++$numTsu;
					if ( !defined($domain) ) { $domain = 'eukarya' }
					elsif ( $domain ne 'eukarya' ) { $domain = 'INCONSISTENT' }
				}
			}

			# SAVE CURRENT
			$prevLeft=$left;
			$prevRight=$right;
			$prevScore=$score;
			$prevName=$name;
			$prevStrand=$strand;
		}
		# COUNT LAST
		if ( $prevName =~ /^(\w+)_rRNA_(\w+)$/ )
		{
			if ( $1 eq 'LSU' ) { ++$numLsu }
			elsif ( $1 eq 'SSU' ) { ++$numSsu }
			if ( !defined($domain) ) { $domain = $2 }
			elsif ( $2 ne $domain ) { $domain = 'INCONSISTENT' }
		} elsif ( $prevName eq '5S_rRNA' )
		{
			++$numTsu;
			if ( defined($domain) and $domain eq 'eukarya' ) { $domain = 'INCONSISTENT' }
		} elsif ( $prevName eq '5_8S_rRNA' )
		{
			++$numTsu;
			if ( !defined($domain) ) { $domain = 'eukarya' }
			elsif ( $domain ne 'eukarya' ) { $domain = 'INCONSISTENT' }
		}

		# SAVE RESULTS
		defined($domain) or $domain = 'UNKNOWN';
		$results{$contig} = [ $numLsu, $numSsu, $numTsu, $domain ];
	}
	$this->{rRNA} = \%results;
	print "Loaded ", scalar(keys %results), " rRNA annotations\n";
}

=item domain

Given a list of contig IDs, return taxonomic domain if all three ribosomal subunits found without disparity; returns empty string otherwise.

=cut

sub domain
{
	my ($this, $contigIds) = @_;
	my $domain = 'UNKNOWN';
	my $numLsu = 0;
	my $numSsu = 0;
	my $numTsu = 0;
	foreach my $contigId ( @$contigIds )
	{
		exists($this->{rRNA}->{$contigId}) or next;
		my ($contigNumLsu,$contigNumSsu,$contigNumTsu,$contigDomain) = @{$this->{rRNA}->{$contigId}};
		if ( $contigDomain eq 'INCONSISTENT' )
		{
			$domain = 'INCONSISTENT';
		}
		elsif ( $contigDomain ne 'UNKNOWN' )
		{
			if ( $domain eq 'UNKNOWN' )
			{
				$domain = $contigDomain;
			} elsif ( $domain ne $contigDomain )
			{
				$domain = 'INCONSISTENT';
			}
		}
		$numLsu += $contigNumLsu;
		$numSsu += $contigNumSsu;
		$numTsu += $contigNumTsu;
	}
	if ( $domain eq 'INCONSISTENT' or $domain eq 'UNKNOWN' ) { $domain = '' }
	elsif ( !$numLsu or !$numSsu or !$numTsu ) { $domain = '' } # all three subunits must have been found to be known
	return wantarray ? ($domain, $numLsu, $numSsu, $numTsu) : $domain;
}

1;

=back

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 COPYRIGHT/LICENSE

Copyright 2017 US DOE Joint Genome Institute.  Use freely under the same terms as Perl itself.

=head1 AUSPICE STATEMENT

The work conducted by the U.S. Department of Energy Joint Genome Institute is supported by the Office of Science of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

=cut

