=pod

=head1 NAME

MetaQC::Param

=head1 DESCRIPTION

Class for binning parameters.

=head1 METHODS

=over 5

=cut

package MetaQC::Parameters;

use strict;

=item new

Create new object without defining parameters (initializing shall be required).  Requires configuration hashref and optionally accepts binning executable (which may alternatively be defined in config).

=cut

sub new
{
	my ($class, $config) = @_;
	$config or die("config required\n");
	my $this =
	{
		config => $config, # configuration hashref with BIN and possibly GA section
		exe => undef, # binning executable used by this object (more than one may be available to class)
		params => {} # parameters used by this object (a set may be available to class if using GA)
	};
	bless $this, $class;
	return $this;
}

=item initFromConfig

Create init object with parameters specified in BIN config section.

=cut

sub initFromConfig
{
	my $this = shift;
	exists($this->{config}->{BIN}) or die("Config missing BIN section\n");
	exists($this->{config}->{BIN}->{exe}) or die("Binning executable not defined in BIN section of config.\n");
	my $exe = $this->{exe} = $this->{config}->{BIN}->{exe};
	exists($this->{config}->{BIN}->{$exe}) or die("Binning parameters not defined in BIN config for $exe.\n");
	my $defn = $this->{config}->{BIN}->{$exe};
	$this->{params}->{$_} = $defn->{$_} foreach keys %$defn;
}

=item initFromTemplate

Create init object using uniform selection of alleles defined in GA config section.

=cut

sub initFromTemplate
{
	my $this = shift;
	my $seed = $this->_initTemplate(1);
	if ( defined($seed) )
	{
		# SEEDED PARAMETERS
		foreach my $key ( keys %{$this->{template}} )
		{
			if ( exists($seed->{$key}) )
			{
				# PARAMETER VALUE WAS DEFINED IN CONFIG FILE
				$this->{params}->{$key} = $seed->{$key};
			} else 
			{
				# PARAMETERS/GENES NOT DEFINED BY SEED CONFIG ARE SET TO RANDOM VALUES/ALLELES
				$this->_set($key);
			}
		}
	} else
	{
		# RANDOM ALLELES
		$this->_set($_) foreach keys %{$this->{template}};
	}
}

=item _initTemplate

Initialize template from configuration; applies only to genetic algorithm.  May return preset parameters (i.e. seeded solutions which are guaranteed to be included in GA population).

=cut

sub _initTemplate
{
	my ($this, $seed) = @_;

	# INIT VARS
	my %seed;
	my %template;

	# DEFINE BINNING EXECUTABLE
	# EXECUTABLE MAY OR MAY NOT BE EXPLICITLY DEFINED (MAY BE PROVIDED BY CONFIG FILE)
	exists($this->{config}->{GA}->{exe}) or die("Binning executable not defined in GA section of config\n");
	my $exe = $this->{exe} = $this->{config}->{GA}->{exe};

	# DEFINE TEMPLATE
	exists($this->{config}->{GA}->{$exe}) or die("Parameter template not defined for $exe in config's GA section\n");
	my $templateHR = $this->{config}->{GA}->{$exe};
	foreach my $key ( keys %$templateHR )
	{
		my ($type, $defn, $seedAR) = @{$templateHR->{$key}};
		my @values;

		# SEEDED SOLUTIONS ARE THE FIRST PARAMETERS TO BE USED WHEN GENERATING A GA POPULATION.  OPTIONAL.
		$seed and $seedAR and @$seedAR and $seed{$key} = shift @$seedAR; # CONSUMED

		# PARSE THE DEFINITION AND POPULATE LIST OF ALLOWED VALUES (ALLELES)
		foreach my $x ( split(/,/, $defn) )
		{
			if ( $type eq 'ENUM' )
			{
				push @values, $x;
			} elsif ( $type eq 'INT' and $x =~ /^\-?\d+$/ )
			{
				push @values, $x;
			} elsif ( $type eq 'INT' and $x =~ /^(\-?\d+)\-(\-?\d+):(\-?\d+)$/ )
			{
				my ($min, $max, $step) = ($1,$2,$3);
				$min < $max or die("Invalid definition of $key: $defn\n");
				$step > 0 or die("Invalid definition of $key: $defn\n");
				for (my $i=$min; $i<=$max; $i += $step ) { push @values, $i }
			} elsif ( $type eq 'INT' and $x =~ /^(\-?\d+)\-(\-?\d+)$/ )
			{
				my ($min, $max, $step) = ($1,$2,1);
				$min < $max or die("Invalid definition of $key: $defn\n");
				for (my $i=$min; $i<=$max; $i += $step ) { push @values, $i }
			} elsif ( $type eq 'FLOAT' and $x =~ /^\-?[\d\.]+$/ )
			{
				push @values, $x;
			} elsif ( $type eq 'FLOAT' and $x =~ /^(\-?[\d\.]+)\-(\-?[\d\.]+):(\-?[\d\.]+)$/ )
			{
				my ($min, $max, $step) = ($1,$2,$3);
				$min < $max or die("Invalid definition of $key: $defn\n");
				$step > 0 or die("Invalid definition of $key: $defn\n");
				for (my $i=$min; $i<=$max; $i += $step ) { push @values, $i }
			} else
			{
				die("Invalid parameter definition for $key: $type|$defn\n");
			}
		}
		$template{$key} = \@values;
	}
	$this->{template} = \%template;
	return scalar(keys %seed) ? \%seed : undef;
}

=item initFromParents

Create init object using uniform selection of alleles from specified parents.

=cut

sub initFromParents
{
	my ($this, $parents) = @_;
	@$parents or die("Cannot init from parents; no parents provided\n");
	exists($this->{config}->{GA}->{exe}) or die("Binning executable not defined in config GA section\n");
	$this->{exe} = $this->{config}->{GA}->{exe};

	# INIT TEMPLATE
	$this->_initTemplate;

	# REPRODUCTION
	my $mutationRate = $this->{config}->{GA}->{mutationRate} // 0.05;
	my $numParents = scalar(@$parents);
	foreach my $parent (@$parents)
	{
		$parent->isa('MetaQC::Parameters') or die("Parent chromosome not a Parameters object\n");
	}
	foreach my $key ( keys %{$this->{template}} )
	{
		if ( rand(1) < $mutationRate )
		{
			# MUTATION
			$this->_set($key);
		} else
		{
			# UNIFORM ALLELE SELECTION
			my $i = int(rand($numParents));
			$this->{params}->{$key} = $parents->[$i]->{params}->{$key};
		}
	}
}

=item initFromId

Given parameter set ID (e.g. bin folder name), set parameters.  Parameters are not validated.

=cut

sub initFromId
{
	my ($this, $id) = @_;
	$id =~ /^(metabat[12]):(.+)$/ or die("Invalid ID: $id\n");
	$this->{exe} = $1;
	my @str = split(/,/, $2);
	my $unnamedParam = 0;
	foreach my $str (@str)
	{
		if ( $str =~ /^(\S+)=(\S+)$/ )
		{
			my $key = length($1) == 1 ? "-$1" : "--$1";
			my $value = $2;
			$this->{params}->{$key} = $value;
		} else
		{
			if ( length($str) == 1 ) { $str = "-$str" }
			elsif ( length($str) > 1 ) { $str = "--$str" }
			my $key = 'UNNAMED'.++$unnamedParam;
			$this->{params}->{$key} = $str;
		}
	}
}	

=item _set

Set a parameter using uniform distribution (i.e. all alleles have equal chance of being selected).

=cut

sub _set
{
	my ($this, $key) = @_;
	my $alleles = $this->{template}->{$key};
	my $i = int(rand(scalar(@$alleles)));
	return $this->{params}->{$key} = $this->{template}->{$key}->[$i];
}

=item cmd

Return executable and parameters as list or string (for execution or naming folders, respectively).

=cut

sub cmd
{
	my $this = shift;
	my @cmd = ( $this->{exe} );
	foreach my $key ( keys %{$this->{params}} )
	{
		my $value = $this->{params}->{$key};
		if ( $key =~ /^\-+/ and $value ne '' )
		{
			# DASHES INDICATE IT'S A FLAG TO BE INCLUDED IN COMMAND
			push @cmd, ($key, $value);
		} else
		{
			# ELSE IT'S JUST A LABEL AND SHOULD NOT BE INCLUDED
			$value =~ /^\-+/ or die("Invalid option: $key => $value\n");
			push @cmd, $value;
		}
	}
	return wantarray ? @cmd : "@cmd";
}

=item id

Return executable and parameters encoded into a string for unique identification (e.g. naming folders).

=cut

sub id
{
	my $this = shift;
	my @params = ();
	foreach my $key ( keys %{$this->{params}} )
	{
		my $value = $this->{params}->{$key};
		if ( $key =~ s/^\-+//g )
		{
			# DASHES INDICATE IT'S A FLAG TO BE INCLUDED IN COMMAND
			push @params, "$key=$value";
		} else
		{
			# ELSE IT'S JUST A LABEL AND SHOULD NOT BE INCLUDED
			$value =~ s/^\-+//g or die("Invalid option: $key => $value\n");
			push @params, $value;
		}
	}
	return $this->{exe}.':'. join(',', sort @params);
}

1;

=back

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 COPYRIGHT/LICENSE

Copyright 2017 US DOE Joint Genome Institute.  Use freely under the same terms as Perl itself.

=head1 AUSPICE STATEMENT

The work conducted by the U.S. Department of Energy Joint Genome Institute is supported by the Office of Science of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

=cut

