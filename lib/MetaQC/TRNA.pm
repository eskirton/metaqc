=pod

=head1 NAME

MetaQC::TRnascan

=head1 DESCRIPTION

Class provides methods for annotating and returning number of tRNAs per contig.

=head1 METHODS

=over 5

=cut

package MetaQC::TRNA;

use strict;
use MetaQC::TRNA::tRNAscan;
use MetaQC::TRNA::cmsearch;

=item new

The config file indicates the method for annotating tRNAs.

=cut

sub new
{
	my $class = shift;
	my $config = @_[0];
	defined($config) or die("Config required\n");
	if ( exists($config->{tRNA}->{db}) )
	{
		print "Using cmsearch to annotate tRNAs\n";
		return new MetaQC::TRNA::cmsearch(@_);
	} else
	{
		print "Using tRNAscan-SE to annotate tRNAs\n";
		return new MetaQC::TRNA::tRNAscan(@_);
	}
}

1;

=back

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 COPYRIGHT/LICENSE

Copyright 2017 US DOE Joint Genome Institute.  Use freely under the same terms as Perl itself.

=head1 AUSPICE STATEMENT

The work conducted by the U.S. Department of Energy Joint Genome Institute is supported by the Office of Science of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

=cut
