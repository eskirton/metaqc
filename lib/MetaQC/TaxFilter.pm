=pod

=head1 NAME

MetaQC::TaxFilter

=head1 DESCRIPTION

Determine lowest common ancestor for each contig, pick consensus phylum of a genome bin, and filter contigs with other phylum annotation.

=head1 METHODS

=over 5

=cut

package MetaQC::TaxFilter;

use strict;
use Env qw(TMPDIR);
use File::Path qw(make_path remove_tree);
use File::Copy;
use Parallel::ForkManager;
use constant { MAX_E_VALUE => 0.1, MIN_PCT_ID => 0.3, MIN_PCT_LEN => 0.7, NUM_TOP_HITS => 5 };

defined($TMPDIR) or die("\$TMPDIR not defined\n");
-d $TMPDIR or die("\$TMPDIR does not exist: $TMPDIR\n");

=item new

Constructor.

=cut

sub new
{
	my ($class, $config, $dir, $contigsFile, $threads, $genesInfile, $proteinsInfile) = @_;

	defined($config) or die("PROTEINS: config hashref required\n");
	ref($config) eq 'HASH' or die("PROTEINS: Config hashref required\n");
	exists($config->{PROTEINS}) or return;
	exists($config->{PROTEINS}->{nr}) or die("Config missing nr location\n");
	$threads or die("Threads required\n");

	$dir or die("PROTEINS: Dir required\n");
	-d $dir or make_path($dir) or die("Unable to mkdir $dir: $!\n");
	$contigsFile or die("PROTEINS: Contigs file required\n");
	my $genesFile = "$dir/genes.fna";
	my $proteinsFile = "$dir/proteins.faa";
	my $hitsFile = "$dir/protVsNr.txt";
	my $lcaFile = "$dir/lca.tsv";

	if ( $genesInfile )
	{
		if ( ! -e $genesFile )
		{
			print "Symlinking provided genes\n";
			symlink($genesInfile, $genesFile) or die($!);
		}
	}
	if ( $proteinsInfile )
	{
		if ( ! -e $proteinsFile )
		{
			print "Symlinking provided proteins\n";
			symlink($proteinsInfile, $proteinsFile) or die($!);
		}
	}

	my $copyRef = $config->{PROTEINS}->{copyRef} ? 1:0;

	# NEW OBJ
	my $this =
	{
		config => $config,
		dir => $dir,
		contigsFile => $contigsFile,
		genesFile => $genesFile,
		proteinsFile => $proteinsFile,
		hitsFile => $hitsFile,
		lcaFile => $lcaFile,
		contigs => {}, # contigId => [taxonomy],
		copyRef => $copyRef,
		threads => $threads
	};
	bless $this, $class;
	return $this;
}

=item init

Associate contigs with LCA -- either by loading precomputed results or running pipeline.

=cut

sub init
{
	my $this = shift;
	scalar(keys %{$this->{contigs}}) and return; # already initialized
	my $lcaFile = $this->{lcaFile};
	if ( -e $lcaFile )
	{
		print "Loading LCAs from $lcaFile\n";
		open(my $in, '<', $lcaFile) or die($!);
		while (<$in>)
		{
			chomp;
			my ($contigId, $tax) = split(/\t/);
			my @tax = split(/,/, $tax);
			$this->{contigs}->{$contigId} = \@tax;
		}
		close($in);
	} else
	{
		$this->_findGenes;
		$this->_copyRef;
		$this->_mapProteins;
	}
}


=item _findGenes

Find protein-coding genes.

=cut

sub _findGenes
{
	my ($this) = @_;

	# INIT VARS
	my $contigsFile = $this->{contigsFile};
	my $genesFile = $this->{genesFile};
	my $proteinsFile = $this->{proteinsFile};

	# CHECK IF DONE
	if ( -e $proteinsFile and -e $genesFile )
	{
		print "Using existing protein-coding genes\n";
		return;
	}
	print "Finding protein-coding genes\n";

	# OPEN FILES
	my @contigsFiles;
	my @genesFiles;
	my @proteinsFiles;
	my @fhs;
	my $numThreads = $this->{threads};
	for ( my $i=1; $i<=$numThreads; ++$i )
	{
		my $tmpfile = "$TMPDIR/contigs.$$.$i.fna";
		open(my $out, '>', $tmpfile) or die($!);
		push @fhs, $out;
		push @contigsFiles, $tmpfile;
		push @genesFiles, "$this->{dir}/genes.$i.fna";
		push @proteinsFiles, "$this->{dir}/proteins.$i.faa";
	}

	# SPLIT CONTIGS
	my $i = $#fhs;
	my $out = $fhs[$i];
	open(my $in, '<', $this->{contigsFile}) or die($!);
	while (<$in>)
	{
		if (/^>/) 
		{
			++$i > $#fhs and $i = 0;
			$out = $fhs[$i];
		}
		print $out $_;
	}
	close($in);
	close($_) foreach @fhs;

	# RUN PRODIGAL
	my $pm = new Parallel::ForkManager($numThreads);
	for ( my $i=0; $i<$numThreads; ++$i )
	{
		$pm->start and next;
		my $contigsFile = $contigsFiles[$i];
		my $genesFile = $genesFiles[$i];
		my $proteinsFile = $proteinsFiles[$i];
		unless ( system("prodigal -d $genesFile -a $proteinsFile -i $contigsFile -q -p meta -o /dev/null") == 0 )
		{
			warn("ERROR: Prodigal failed: $!\n");
			unlink($proteinsFile, $genesFile, $contigsFile);
			$pm->finish;
		}
		$pm->finish;
	}
	$pm->wait_all_children;

	# VALIDATE OUPUT
	my $ok = 1;
	for ( my $i=0; $i<$numThreads; ++$i )
	{
		-e $genesFiles[$i] or $ok = 0;
		-e $proteinsFiles[$i] or $ok = 0;
	}
	if ($ok)
	{
		_cat(\@genesFiles, $this->{genesFile});
		_cat(\@proteinsFiles, $this->{proteinsFile});
	} else
	{
		warn("Prodigal failed\n");
	}
	unlink(@contigsFiles, @genesFiles, @proteinsFiles);
}

=item _cat

Concatenate files.  Nonmember function.

=cut

sub _cat
{
	my ($infilesAR, $outfile) = @_;
	open(my $out, '>', $outfile) or die($!);
	foreach my $infile (@$infilesAR)
	{
		open(my $in, '<', $infile) or die($!);
		while (<$in>) { print $out $_ }
		close($in);
	}
	close($out);
}

=item _copyRef

Copy reference db and associated files to TMPDIR, which may be local scratch disk, for speed.

=cut

sub _copyRef
{
	my $this = shift;

	# INIT DEFAULT
	my $nr = $this->{nr} = $this->{config}->{PROTEINS}->{nr};
	my $mapFile = $this->{mapFile} = $this->{config}->{PROTEINS}->{mapFile};
	my $taxFile = $this->{taxFile} = $this->{config}->{PROTEINS}->{taxFile};
	$this->{copyRef} or return;

	# DEFINE PATHS
	my @nr = split(/\//, $nr);
	my $dbName = $nr[$#nr];
	my $localNrDir = "$TMPDIR/$dbName";
	my $blockFile = "$localNrDir/IN_PROGRESS";
	$this->{nr} = "$localNrDir/$dbName";
	$mapFile =~ /\/(\w+\.txt)$/ or die("Error parsing map filename: $mapFile\n");
	my $localMapFile = $this->{mapFile} = "$localNrDir/$1";
	$taxFile =~ /\/(\w+\.txt)$/ or die("Error parsing tax filename: $taxFile\n");
	my $localTaxFile = $this->{taxFile} = "$localNrDir/$1";

	# UPDATE REQUIRED -- PREVIOUSLY DIDN'T COPY MAP AND TAX FILES
	my $doneFile = "$localNrDir/done";
	if ( -e $doneFile )
	{
		print "Copying tax/map files to local scratch\n";
		copy($mapFile, $localMapFile) or die("Unable to cp $mapFile $localMapFile: $!");
		copy($taxFile, $localTaxFile) or die("Unable to cp $taxFile $localTaxFile: $!");
		unlink($doneFile);
	}

	# CHECK IF PRESENT
	if ( -d $localNrDir )
	{
		if ( -e $blockFile )
		{
			# COPY IN PROGRESS (BY ANOTHER PROCESS)
			for ( my $i=0; $i<15; ++$i )
			{
				print "Waiting for NR to copy\n";
				sleep(60);
				-e $blockFile or last;
			}
			-e $blockFile and die("Error copying nr: timeout\n");
		}
		return;
	}

	# COPY
	print "Copying NR to local disk: $localNrDir\n";
	make_path($localNrDir) or die("Unable to mkdir $localNrDir: $!\n");
	open(my $out, '>', $blockFile) or die("Unable to write $blockFile: $!\n");
	close($out);
	system("cp ${nr}* $localNrDir") == 0 or die("Error copying nr: $!\n");
	open(my $out, '>', $doneFile) or die($!);
	close($out);
	copy($mapFile, $localMapFile) or die("Unable to cp $mapFile $localMapFile: $!");
	copy($taxFile, $localTaxFile) or die("Unable to cp $taxFile $localTaxFile: $!");
	unlink($blockFile);
}

=item _mapProteins

Map proteins vs reference db (i.e. NR).  Filter hits with (1) e-value greater than 0.1, (2) less than 30% identity, or (3) less than 70% query alignment length; keep only up to five best hits per protein and save the lowest common ancestor per protein.

=cut

sub _mapProteins
{
	my $this = shift;
	print "Aligning proteins vs NR\n";

	# FILES
	my $proteinsFile = $this->{proteinsFile};
	-e $proteinsFile or die("Proteins file not found: $proteinsFile\n");
	my $nr = $this->{nr};
	my $hitsFile = $this->{hitsFile};
	my $lcaFile = $this->{lcaFile};

	# SEARCH AND FILTER RESULTS, KEEP ONLY TOP FIVE HITS PER PROTEIN.
	# EACH HIT SPANS TWO LINES
	my @row;
	my @hits; # hit taxARs for current protein (up to max num hits)
	my ($queryId, $tmpQueryId, $rec);
	my $in;
	my $out;
	my $force = 1; # DEBUG
	if ( -s $hitsFile and ! $force )
	{
		warn("Using existing NR hits file\n");
	} else
	{
		# COPY PROTEINS FILE TO TMPDIR
		my $tmpProtFile = $proteinsFile;
		if ( $this->{copyRef} )
		{
			$tmpProtFile = "$TMPDIR/$$.".time.".faa";
			copy($proteinsFile, $tmpProtFile) or die("Error copying proteins file to \$TMPDIR\n");
		}

		my $start = time;
		my $cmd = "lastal -E".MAX_E_VALUE." -fBlastTab+ -P$this->{threads} $nr $tmpProtFile";
		open($in, "$cmd |") or die("Unable to execute lastal:\n$cmd\n$!\n");
		open($out, '>', "$hitsFile.tmp") or die("Unable to write $hitsFile: $!\n");
		while (<$in>)
		{
			/^#/ and next;
			@row = split(/\t/);
			if ( scalar(@row) == 2 )
			{
				# FIRST LINE OF TWO
				$rec = $_; # first line of two-line record
				$tmpQueryId = $row[0];

				if ( $tmpQueryId ne $queryId )
				{
					# NEXT PROTEIN
					print $out $_ foreach @hits;
					# INIT NEXT
					$queryId = $tmpQueryId;
					@hits = ();
				}
				# ELSE MORE HITS FOR SAME PROTEIN
			} elsif ( @hits < NUM_TOP_HITS )
			{
				# SECOND LINE OF TWO
				$rec .= $_; # second line of two-line record
				#($empty, $pctIdent, $alLen, $mis, $gap, $qSt, $qEnd, $sSt, $sEnd, $eValue, $bits, $qLen, $sLen, $rawScore) = @row;

				# FILTER POOR ALIGNMENTS
				$row[1] < MIN_PCT_ID and next;
				$row[9] > MAX_E_VALUE and next;
				$row[2]/$row[11] < MIN_PCT_LEN and next;

				# SAVE HIT
				push @hits, $rec;
			}
		}
		close($in);
		print $out $_ foreach @hits;
		close($out);
		move("$hitsFile.tmp", $hitsFile) or die("Error mv $hitsFile.tmp $hitsFile: $!");
		$this->{copyRef} and unlink($tmpProtFile);
		my $dur = int((time - $start)/60 + 0.5);
		print "lastal done after $dur min\n";
	}

	print "Picking lowest common ancestor per contig\n";
	# LOAD TAXONOMIES
	my %taxonomy; # taxId => taxAR
	my $taxFile = $this->{taxFile};
	open($in, '<', $taxFile) or die("Unable to read $taxFile: $!\n");
	while (<$in>)
	{
		chomp;
		my ($taxId, $taxStr) = split(/\t/);
		my @tax = split(/;/, $taxStr);
		$taxonomy{$taxId} = \@tax;
	}
	close($in);

	# LOAD MAP FILE
	my %subjToTax; # subjId => taxAR
	my $mapFile = $this->{mapFile};
	open($in, '<', $mapFile) or die("Error reading $mapFile: $!");
	while (<$in>)
	{
		chomp;
		my ($subjId, $acc, $ko, $ec, $taxId) = split(/\t/);
		$subjToTax{$subjId} = $taxonomy{$taxId};
	}
	close($in);

	# PARSE LASTAL OUTPUT; RE-DO FILTERS IN CASE RESULTS WERE IMPORTED RATHER THAN GENERATED ABOVE
	open($in, '<', $hitsFile) or die("Unable to read $hitsFile: $!\n");
	my @proteins; # LCA taxARs for all proteins on this contig
	my ($subjId, $contigId, $tmpContigId);
	@hits = ();
	while (<$in>)
	{
		/^#/ and next;
		chomp;
		@row = split(/\t/);
		if ( scalar(@row) == 2 )
		{
			# FIRST LINE OF TWO
			($tmpQueryId, $subjId) = @row;
			$tmpQueryId =~ /^(.+)_\d+$/ or die("Error parsing query $tmpQueryId\n");
			$tmpContigId = $1;

			if ( $tmpQueryId ne $queryId )
			{
				# NEXT PROTEIN
				my $protTaxAR = $this->lca(\@hits);
				push @proteins, $protTaxAR;
				$queryId = $tmpQueryId;
				@hits = ();

				if ( $contigId ne $tmpContigId )
				{
					# NEXT CONTIG.  SAVE LAST CONTIG
					defined($contigId) and $this->{contigs}->{$contigId} = $this->lca(\@proteins);
					$contigId = $tmpContigId;
					@proteins = ();
				}
				# ELSE SAME CONTIG
			}
			# ELSE MORE HITS FOR SAME PROTEIN
		} elsif ( @hits < NUM_TOP_HITS )
		{
			# SECOND LINE OF TWO
			#($empty, $pctIdent, $alLen, $mis, $gap, $qSt, $qEnd, $sSt, $sEnd, $eValue, $bits, $qLen, $sLen, $rawScore) = @row;

			# FILTER POOR ALIGNMENTS
			$row[1] < MIN_PCT_ID and next;
			$row[9] > MAX_E_VALUE and next;
			$row[2]/$row[11] < MIN_PCT_LEN and next;

			# SAVE HIT
			#my $hitTaxAR = $subjToTax{$subjId};
			push @hits, $subjToTax{$subjId};
		}
	}
	close($in);
	# SAVE LAST
	defined($contigId) and $this->{contigs}->{$contigId} = $this->lca(\@proteins);

	# DETERMINE LCA FOR EACH PROTEIN
	print "Writing LCA per contig\n";
	open($out, '>', $lcaFile) or die("Unable to write $lcaFile: $!\n");
	foreach my $contigId ( keys %{$this->{contigs}} )
	{
		my $lca = $this->{contigs}->{$contigId};
		print $out $contigId, "\t", join(',', @$lca), "\n";
	}
	close($out);
}

=item lca

Given list of taxonomies, return lowest common ancestor.

=cut

sub lca
{
	my ($this, $taxonomies) = @_;
	scalar(@$taxonomies) < 3 and return [];
	my $min = scalar(@$taxonomies)/2;
	my @lca;
	for (my $rank=0; $rank<8; $rank++)
	{
		my $name;
		my %counts;
		my $consensus;
		foreach my $taxAR (@$taxonomies)
		{
			$rank > $#$taxAR and next;
			$name = $taxAR->[$rank];
			if ( ++$counts{$name} > $min )
			{
				$consensus = $name;
				last;
			}
		}
		defined($consensus) or last;
		push @lca, $consensus;
	}
	return \@lca;
}

=item taxString

Format taxonomy string from arrayref.

=cut

sub taxString
{
	my ($this, $ar) = @_;
	my @levels = qw(d p c o f g s);
	my @tax;
	for ( my $i=0; $i<=$#$ar; ++$i )
	{
		my $l = $levels[$i];
		my $t = $ar->[$i];
		push @tax, $l.'__'.$t;
	}
	return join(';', @tax);
}

=item filter

Given list of contig IDs in a genome bin, returns bin phylum and hash of contig IDs which do not match bin's phylum and their phylum.

METHOD: Once all contigs have taxonomy assignment, score all contigs in a bin based on phylum-level assignments and assign bin based on majority of contig assignments. Remove those contigs that are a different phylum compared to the assignment. 

=cut

sub filter
{
	my ($this, $contigIds) = @_;
	$this->init;

	# DETERMINE LCA FOR BIN
	my @contigTax;
	foreach my $contigId ( @$contigIds )
	{
		my $taxAR = $this->{contigs}->{$contigId};
		defined($taxAR) or next;
		scalar(@$taxAR) or next;
		push @contigTax, $taxAR;
	}
	my $lcaAR = $this->lca(\@contigTax);
	defined($lcaAR) or return;
	my $level;
	if ( @$lcaAR > 1 ) { $level = 1 }
	elsif ( @$lcaAR == 1 ) { $level = 0 }
	else { return }

	my %filtered;
	my $numFiltered = 0;
	foreach my $contigId (@$contigIds)
	{
		exists($this->{contigs}->{$contigId}) or next;
		my $taxAR = $this->{contigs}->{$contigId};
		my $t;
		if ( $level == 1 )
		{
			@$taxAR > 1 or next;
			$t = join(';', @$taxAR[0,1]);
		} elsif ( $level == 0 )
		{
			@$taxAR > 0 or next;
			$t = $taxAR->[0];
		}
		$lcaAR->[$level] eq $taxAR->[$level] and next;
		$filtered{$contigId} = $this->taxString($taxAR);
		++$numFiltered;
	}

	my $lca = $this->taxString($lcaAR);
	return ($lca, \%filtered, $numFiltered);

#	my %phyla;
#	my $n=0;
#	foreach my $contigId ( @$contigIds )
#	{
#		my $taxAR = $this->{contigs}->{$contigId};
#		defined($taxAR) or next;
#		scalar(@$taxAR) or next;
#		my ($d, $p) = @$taxAR;
#		++$phyla{$p};
#		++$n;
#	}
#
#	# PICK PHYLUM
#	my $min = $n/2; # more than half of the contigs must have this phylum in common
#	my $phylum; # consensus phylum for this bin
#	foreach my $p ( keys %phyla )
#	{
#		$phyla{$p} < $min and next;
#		$phylum = $p;
#		last;
#	}
#
#	# FILTER
#	my %filtered;
#	my $numFiltered = 0;
#	if ( $phylum )
#	{
#		foreach my $contigId (@$contigIds)
#		{
#			exists($this->{contigs}->{$contigId}) or next;
#			my $taxAR = $this->{contigs}->{$contigId};
#			scalar(@$taxAR) or next;
#			my $p = $taxAR->[0];
#			$p eq $phylum and next;
#			$filtered{$contigId} = $p;
#			++$numFiltered;
#		}
#	}
#	return ($phylum, \%filtered, $numFiltered);
}


1;

=back

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 COPYRIGHT/LICENSE

Copyright 2017 US DOE Joint Genome Institute.  Use freely under the same terms as Perl itself.

=head1 AUSPICE STATEMENT

The work conducted by the U.S. Department of Energy Joint Genome Institute is supported by the Office of Science of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

=cut


__END__

NOTES:

ALGORITHM DESCRIPTION OF "PhyloDistQC" TOOL RECEIVED FROM IMG TEAM, WHICH I'VE USED FOR CONSISTENCY SAKE:

1. Last all genes on all contigs against the IMG NR db.
2. Remove everything with e-value greater than 0.1
3. Filter based on top 5 hits, 30% ID for at least 70% query gene length to assign taxonomy lineage on a per-gene basis.
4. Once you have taxonomy assignments for all genes on a contig, use the below assignment for the contigs: 


(For each contig ...)

For each phylogenetic level p in the following order (species, genus, family, ..., phylum, domain )
do
    If number of genes on contig >= 10
    then
        if at least 51% of genes represent one named entity in p
             Pick that as origin
        fi
    elsif number of genes on contig < 10
    then
        if BlastP and at least 3 genes represent one named entity in p
             Pick that as origin
        fi
    fi
done

This has been refined a few times and our current algorithm is documented as:

-- Start from most specific lineage specification and go
   to least specific.
-- For each level of specificty, find the lineage with the
   that occurs most commonly. Record it's number of phylogenetic
    distribution hits.
-- For contigs having >= 10 or <= 3 genes
   stop and return the lineage if the percentage of hits for
   for this lineage >= 51%.
-- Else if the contig has between 4 and 9 genes
   stop and return the lineage if there are >= 3 hits
   for that lineage.  (This is a heuristic hack based on
   observations about the data.)

6. Once all contigs have taxonomy assignment, score all contigs in a bin based on phylum-level assignments and assign bin based on majority of contig assignments. Remove those contigs that are a different phylum compared to the assignment. 
(For each contig ...)

