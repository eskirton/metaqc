#!/usr/bin/env perl

=pod

=head1 NAME

metaQc.pl

=head1 SYNOPSIS

metaQc.pl --contigs ./contigs.fna

=head1 DESCRIPTION

Evaluate metagenome bins and categorize as 'high', 'medium', or 'low' quality.

=head1 OPTIONS

=over 5

=item --config C<path>

Configuration file in I<JSON> format.

=item --out C<path>

Location for genome bins and QC outfiles.  Optional; default=cwd.

=item --contigs C<path>

Contigs FASTA.  Reqiured.

=item --cov C<path>

Read coverage table (contig ID, coverage).

=item --name C<string>

Assembly name, used for naming outfiles.  Optional; if not provided then will be derived from output folder name.

=item --cmsearch C<path>

Cmsearch rRNA annotations.  Optional; if provided, all contigs must have been searched.

=item --trnascan C<path>

tRNAscan-SE tRNA annotations.  Optional; if provided, all contigs must have been searched.

=item --metabat C<path>

Optionally provide MetaBAT input dir if wish to use previous binning results.  Otherwise MetaBAT shall be run.

=item --threads C<int>

Number of threads to use.  Optional; default check nproc and use all.  Minimum=8.

=item --mem C<int|string>

Maximum available RAM in GB (e.g. 120 or 120G).  Optional; default check /proc/meminfo and use 90% of MemTotal.  Minimum=48G.

=back

=head1 SEE ALSO

MetaBAT, CheckM, RefineM, cmsearch, tRNAscan-SE, HMMer, TrimAl, FastTree, LAST.

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 COPYRIGHT

Copyright 2017 by the United States Department of Energy Joint Genome Institute.  Use freely under the same terms as Perl itself.

=head1 AUSPICE STATEMENT

The work conducted by the U.S. Department of Energy Joint Genome Institute is supported by the Office of Science of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

=cut

use strict;
use warnings;
use Env qw(TMPDIR METAQC_DIR METAQC_CONFIG_FILE);
use Getopt::Long;
use Pod::Usage;
use MetaQC::Metagenome;
use File::Spec;
use File::Basename;

$TMPDIR or die("\$TMPDIR not defined\n");
-d $TMPDIR or die("\$TMPDIR = $TMPDIR does not exist\n");

# OPTIONS
my $threads = 0;
my $ram = 0;
my $contigsFile;
my $covFile;
my $dir='.';
my $name;
my $configFile = $METAQC_CONFIG_FILE;
my $cmsearchFile;
my $tRnascanFile;
my $metabatDir;
my $debug;
my $help;
my $man;
GetOptions(
	'threads=i' => \$threads,
	'mem=s' => \$ram,
    'contigs=s' => \$contigsFile,
	'cov=s' => \$covFile,
	'config=s' => \$configFile,
	'cmsearch=s' => \$cmsearchFile,
	'tRnascan=s' => \$tRnascanFile,
	'metabat=s' => \$metabatDir,
    'out=s' => \$dir,
	'name=s' => \$name,
	'debug' => \$debug,
    'help|?' => \$help,
    'man' => \$man
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;
$threads or $threads = `nproc` + 0;
$dir = File::Spec->rel2abs($dir);
$configFile or die("--config file required\n");
$configFile = File::Spec->rel2abs($configFile);
$contigsFile or $contigsFile = "$dir/contigs.fna";
$contigsFile = File::Spec->rel2abs($contigsFile);
if ( $covFile )
{
	$covFile = File::Spec->rel2abs($covFile);
} else
{
	$covFile = "$dir/coverage.metabat.tsv";
	-f $covFile or $covFile = '';
}
$threads or $threads = int(`nproc`);
$threads < 8 and die("At least 8 threads are required\n");
if ( $ram )
{
	if ( $ram =~ /^(\d+)G?$/ ) { $ram = $1 }
	else { die("Invalid --mem parameter: $ram (expected RAM in GB as: <int>[G])\n") }
} else
{
	my $line = `grep '^MemTotal' /proc/meminfo`;
	$line =~ /^MemTotal:\s+(\d+) kB/ or die("Error parsing /proc/meminfo\n");
	$ram = int($1/1024/1024*0.8);
}
$ram >= 48 or die("A minimum of 48G RAM required\n");
$name or $name = fileparse($dir);
print "Begin processing assembly $name\n";


# MAIN
my $start = time;

#
# check that all executables are in $PATH
#
my $exe_out = `which cmsearch`;
if (! $exe_out ){
        die "cmsearch not found in \$PATH\n";
}

$exe_out = `which checkm`;
if (! $exe_out ){
        die "checkm not found in \$PATH\n";
}

$exe_out = `which tRNAscan-SE`;
if (! $exe_out ){
        die "tRNAscan-SE not found in \$PATH\n";
}

my $metagenome = MetaQC::Metagenome->new($configFile, $dir, $name, $contigsFile, $covFile, $threads, $ram, $metabatDir, $cmsearchFile, $tRnascanFile);
$debug and $metagenome->summary;
my $sec = time - $start;
my $hr = int($sec/3600);
my $min = int($sec/60) - $hr*60;
$sec = $sec - $hr*3600 - $min*60;
print "Done after $hr:$min:$sec h:m:s\n";
exit;

