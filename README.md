# README #

metaqc

### What is this repository for? ###

This tools will categorize the quality of metagenome bins based on the published criteria in:
"Minimum information about a single amplified genome (MISAG) and a
metagenome-assembled genome (MIMAG) of bacteria and archaea"
 [link](http://www.nature.com/nbt/journal/v35/n8/full/nbt.3893.html?foxtrotcallback=true)

In short, this tool labels bins as high, medium, or low quality based on their completeness(checkM), contamination(checkM), number of tRNAs(tRNAscan-SE), and ribosomal RNAs(infernal).

The above paper defines the scoring system which is called *Minimum Information about a Metagenome-Assembled Genome (MIMAG)*

### Category definitions
genBinQc.pl will give a high, medium or low quality label to each bin.  The scoring system is based on the following criteria.  

#### high quality:  
```
  >=90% complete
  <=5% contamination
  >=18 tRNAs (21 total)
  >=1(23S) + >=1(16S) + >=1(5S) rRNA and all subunits must be from same taxonomic domain
```

#### medium:  
```
  >= 50% complete, <= 10% contam  
```

#### low:  
```
  < 50% complete, <= 10% contam  
```

## How do I get set up? ###
### 1. Install the database
* You need to download the (database). See INSTALL.md  

### 2. Install required dependencies
See INSTALL.md under the "Source" tab. 

## Running QC
to see options  
`genBinQc.pl --help`

## Example
you can use the test directory with the provided fasta file
genBinQc.pl --dir <repository>/test --level 3 --id test

## The Results
There will be a table labeling each bin with it's quality as well as completeness(%), tRNAs found, rRNAs found, and contamination(%).  
The files with the actual tRNA and rRNA fasta's should also be present.

## Who do I talk to? ###

* eskirton at lbl.gov built the qc tool.
* jlfroula at lbl.gov built the Makefile and is owner of the repository