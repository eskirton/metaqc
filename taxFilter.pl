#!/usr/bin/env perl

=pod

=head1 NAME

taxFilter.pl

=head1 SYNOPSIS

taxFilter.pl --config conf.json --dir . --contigs ./contigs.fna --threads 16

=head1 DESCRIPTION

To discard contigs which don't match the phylum of the bin.  Given a set of contigs putatively assigned to a single genome, determine the lowest common ancestor of each coding gene considering it's proteins' five acceptable top hits vs nr, determine the most abundant LCA for the contig, determine the most abundant phylum for the bin, and filter contigs which do not match the assigned phylum.

=head1 OPTIONS

=over 5

=item --config C<path>

Config file in JSON format.  Required; includes reference db paths.

=item --contigs C<path>

All contigs in FASTA format.

=item --in C<path>

Input folder containing one or more genome bins (using the naming convention: <ID>.<binNum>.fa); output shall be written to C<filtered> subdir.

=item --out C<path>

Output folder containing filtered bins.  Optional; default=<indir>/filtered

=item --genes C<path>

Annotated coding genes in FASTA format.  Optional; if not provided, Prodigal will be run to generate it.

=item --proteins C<path>

Protein sequences in FASTA format.  Optional; if not provided, Prodigal will be run to generate it.

=item --threads C<int>

Number of threads to use.  Optional; default=8.

=back

=head1 SEE ALSO

Prodigal, LAST

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 COPYRIGHT

Copyright 2017 by the United States Department of Energy Joint Genome Institute.

=head1 AUSPICE STATEMENT

The work conducted by the U.S. Department of Energy Joint Genome Institute is supported by the Office of Science of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

=cut

use strict;
use warnings;
use Getopt::Long;
use Pod::Usage;
use File::Path qw(make_path remove_tree);
use MetaQC::TaxFilter;
use File::Slurp;
use JSON;
use File::Copy;

# OPTIONS
my $configFile;
my $contigsFile;
my $genesInfile;
my $proteinsInfile;
my $indir;
my $outdir;
my $threads=8;
my $help;
my $man;
GetOptions(
    'config=s' => \$configFile,
	'contigs=s' => \$contigsFile,
    'in=s' => \$indir,
	'out=s' => \$outdir,
	'genes=s' => \$genesInfile,
	'proteins=s' => \$proteinsInfile,
	'threads=i' => \$threads,
    'help|?' => \$help,
    'man' => \$man
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;
$configFile or die("--config file required\n");
-e $configFile or die("--config $configFile does not exist\n");
$contigsFile or die("--contigs file required\n");
-f $contigsFile or die("--contigs $contigsFile does not exist\n");
$indir or die("--dir required\n");
-d $indir or die("--dir $indir does not exist\n");
$threads or die("--threads must be >0\n");
if ( $genesInfile and ! -e $genesInfile ) { die("--genes $genesInfile does not exist\n") }
if ( $proteinsInfile and ! -e $proteinsInfile ) { die("--proteins $proteinsInfile does not exist\n") }
$outdir or $outdir = "$indir/filtered";
-d $outdir or mkdir($outdir) or die("Unable to mkdir $outdir: $!\n");
my $outliersFile = "$outdir/outliers.tsv";

# CONFIG
my $txt = read_file($configFile);	
my $config = from_json($txt);

# LOAD BINS
print "Loading bins\n";
opendir(DIR, $indir) or die("Unable to readdir $indir: $!\n");
my @binFiles = grep { /^\S+\.\d+\.(fa|fna|fasta)$/ } readdir(DIR);
closedir(DIR);
print "\tThere are ", scalar(@binFiles), " bins\n";
my %bins;
my %binFiles;
my @allContigs;
foreach my $file (@binFiles)
{
	$file =~ /^(\S+\.\d+)\.(fa|fna|fasta)$/ or die;
	my $binId = $1;
	$binFiles{$binId} = "$indir/$file";
	my @contigs;
	open(my $in, '<', "$indir/$file") or die($!);
	while (<$in>)
	{
		/^>(\S+)/ or next;
		push @contigs, $1;
		push @allContigs, $1;
	}
	close($in);
	$bins{$binId} = \@contigs;
}

# MAIN
my $tf = new MetaQC::TaxFilter($config, $indir, $contigsFile, $threads, $genesInfile, $proteinsInfile);
$tf->annotate(\@allContigs);

# IDENTIFY OUTLIER CONTIGS
my $numFiltered = 0;
open(my $tab, '>', "$outliersFile.tmp") or die($!);
foreach my $binId ( keys %bins )
{
	# TAX FILTER
	my $fastaInfile = "$indir/$binId.fa";
	my $fastaOutfile = "$outdir/$binId.fa";
	-e $fastaOutfile and unlink($fastaOutfile);
	my ($phylum, $filteredHR) = $tf->filter($bins{$binId});
	my @filtered = keys %$filteredHR;
	my $n = scalar(@filtered);
	if ( $phylum and $n )
	{
		# FILTER CONTIGS
		$numFiltered += $n;
		foreach my $contigId (@filtered)
		{
			print $tab join("\t", $binId, $phylum, $contigId, $filteredHR->{$contigId}), "\n";
		}
		my $isFiltered;
		open(my $in, '<', $fastaInfile) or die($!);
		open(my $out, '>', $fastaOutfile) or die($!);
		while (<$in>)
		{
			if (/^>(\S+)/)
			{
				$isFiltered = exists($filteredHR->{$1}) ? 1:0;
			}
			if ( ! $isFiltered )
			{
				print $out $_;
			}
		}
		close($in);
		close($out);
	} else
	{
		# NO CONTIGS REMOVED; SYMLINK FASTA
		symlink($fastaInfile, $fastaOutfile);
	}
}
close($tab);
move("$outliersFile.tmp", $outliersFile);
exit;
