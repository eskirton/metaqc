#!/bin/bash
set -x
set -e

module=$1
dir=$2
if [[ ! $module ]] || [[ ! $dir ]]; then
	echo "Usage: $0 <https module path> <tool dir name>"
	exit
fi
mybasename=$(basename $module)
myhome=$(pwd)

mkdir -p CPAN/$dir
wget --no-check-certificate $module --output-document CPAN/$dir/$mybasename
tar zxf CPAN/$dir/$mybasename  --directory CPAN/$dir --strip-components=1
cd CPAN/$dir
perl Makefile.PL INSTALL_BASE=/homes/oakland/jfroula/KBASE/metaqc/lib
cd $myhome
make --directory=CPAN/$dir/
#make test --directory=CPAN/$dir/
make install --directory=CPAN/$dir/
